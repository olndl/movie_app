# FlutterDevPost App
Flutter app that displays a list of movies obtained from [themoviedb](https://developers.themoviedb.org/3/getting-started/introduction) API.

## Features
#### Basic
- [x] Screens: movies list, movie detals, favorites movies
- [x] Work with backend is implemented, data is received from the server
- [X] State-management and DI implemented using `flutter_riverpod`
- [X] Implemented Navigation
- [x] Organized saving data to disk using a SQFLite
- [X] Offline-first mood


## Video
https://drive.google.com/file/d/1zioZjrjTp-yIFu7MRuIKse3WBT5HfHR5/view?usp=sharing

## Screenshots
<p float="left">
<img src="screenshots/Screenshot1.png" width="200" height="400"/>
<img src="screenshots/Screenshot2.png" width="200" height="400"/>
<img src="screenshots/Screenshot3.png" width="200" height="400"/>
<img src="screenshots/Screenshot4.png" width="200" height="400"/>
<img src="screenshots/Screenshot5.png" width="200" height="400"/>
</p>

## Get Started
Generate file *.g.dart and *.freezed.dart, you can use this command on terminal :

`flutter pub run build_runner build --delete-conflicting-outputs`

## Packages Used

- `flutter_riverpod` for state management.
- `dio` to work with Http client.
- `freezed` - code generator for data-classes
- `sqflite` and `path_provider` to support local storage.
- `linter` - for code rules.
- more at `pubspec.yaml`
