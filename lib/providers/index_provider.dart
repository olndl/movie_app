import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final indexProvider = ChangeNotifierProvider.family<IndexProvider, int>(
  (_, index) => IndexProvider(),
);

class IndexProvider with ChangeNotifier {
  int currentIndex;

  IndexProvider({this.currentIndex = 0});

  int get getCurrentIndex => currentIndex;

  indexSet(int ind) {
    currentIndex = ind;
    notifyListeners();
  }
}
