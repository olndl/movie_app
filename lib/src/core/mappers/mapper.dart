import 'package:movie_app/src/core/utils/utils.dart';
import 'package:movie_app/src/features/domain/models/movies_model.dart';

class Mapper {
  MovieModel transformToMovieModel(Map<String, dynamic> entity) {
    return MovieModel(
      adult: entity['adult'] == 1,
      backdropPath: entity['backdrop_path'],
      id: entity['id'],
      originalLanguage: entity['original_language'],
      originalTitle: entity['original_title'],
      overview: entity['overview'],
      popularity: entity['popularity'],
      posterPath: entity['poster_path'],
      releaseDate: Utils.fromTimestampToDatetime(entity['release_date']),
      title: entity['title'],
      video: entity['video'] == 1,
      voteAverage: entity['vote_average'],
      voteCount: entity['vote_count'],
      isFavorite: entity['is_favorite'] == 1,
    );
  }

  Map<String, dynamic> transformToMap(MovieModel movieModel) {
    return {
      'adult': movieModel.adult != null
          ? movieModel.adult!
              ? 1
              : 0
          : null,
      'backdrop_path': movieModel.backdropPath,
      'id': movieModel.id,
      'original_language': movieModel.originalLanguage,
      'original_title': movieModel.originalLanguage,
      'overview': movieModel.overview,
      'popularity': movieModel.popularity,
      'poster_path': movieModel.posterPath,
      'release_date': Utils.fromDatetimeToTimestamp(movieModel.releaseDate),
      'title': movieModel.title,
      'video': movieModel.video != null
          ? movieModel.video!
              ? 1
              : 0
          : null,
      'vote_average': movieModel.voteAverage,
      'vote_count': movieModel.voteCount,
      'is_favorite': movieModel.isFavorite ? 1 : 0
    };
  }
}
