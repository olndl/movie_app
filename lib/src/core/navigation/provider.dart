import 'package:movie_app/src/core/navigation/delegate.dart';
import 'package:movie_app/src/core/navigation/model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final routerDelegateProvider = Provider<MovieRouterDelegate>(
  (ref) => MovieRouterDelegate(ref, [ListMoviesSegment()]),
);

final navigationStackProvider =
    StateProvider<TypedPath>((_) => [ListMoviesSegment()]);
