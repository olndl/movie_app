import 'dart:convert';
import 'package:movie_app/src/features/domain/models/movies_model.dart';

typedef JsonMap = Map<String, dynamic>;

abstract class TypedSegment {
  factory TypedSegment.fromJson(JsonMap json) {
    if (json['path'] == 'ListMoviesSegment') {
      return ListMoviesSegment();
    }
    if (json['path'] == 'MovieDetailsSegment') {
      return MovieDetailsSegment(movie: json['movie']);
    }
    if (json['path'] == 'ListFavoriteSegment') {
      return ListFavoriteSegment();
    }
    return ListMoviesSegment();
  }

  JsonMap toJson() => <String, dynamic>{'path': runtimeType.toString()};

  @override
  String toString() => jsonEncode(toJson());
}

typedef TypedPath = List<TypedSegment>;

class ListMoviesSegment with TypedSegment {}

class MovieDetailsSegment with TypedSegment {
  MovieDetailsSegment({required this.movie});

  final MovieModel movie;
}

class ListFavoriteSegment with TypedSegment {}
