class Endpoints {
  Endpoints._();
  static const String contentType = 'application/json';
  static const String baseUrl =
      'https://api.themoviedb.org/3/movie/popular?api_key=05a1b3bd65983ae77f404cfeeb9007bc';
  static const String noImageUrl =
      'https://storage.googleapis.com/cms-storage-bucket/d406c736e7c4c57f5f61.png';
  static const String imageUrl = 'https://image.tmdb.org/t/p/w500/';
  //static const String popular = '$baseUrl/popular';
  static const String host = 'example.com';
}
