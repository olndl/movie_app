import 'package:movie_app/src/core/theme/colors_guide.dart';
import 'package:flutter/material.dart';
import 'package:movie_app/src/gen/fonts.gen.dart';

abstract class TextStyles {
  static TextStyle title = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
    color: ColorsGuide.titleColor,
    fontFamily: FontFamily.cormorantGaramondBold,
  );
  static TextStyle body = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.normal,
    color: ColorsGuide.titleColor,
    fontFamily: FontFamily.cormorantGaramondMedium,
  );
  static TextStyle smallBody = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.normal,
    color: ColorsGuide.labelColor,
    fontFamily: FontFamily.cormorantGaramondMedium,
  );
}
