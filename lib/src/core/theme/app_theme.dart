import 'package:movie_app/src/core/theme/colors_guide.dart';
import 'package:flutter/material.dart';
import 'package:movie_app/src/gen/fonts.gen.dart';

class AppTheme {
  static const _defaultFontFamily = FontFamily.cormorantGaramondMedium;

  static ThemeData get lightTheme {
    return ThemeData(
      brightness: Brightness.light,
      fontFamily: _defaultFontFamily,
      colorScheme: ColorScheme.fromSwatch()
          .copyWith(secondary: ColorsGuide.backgroundColor),
    );
  }
}
