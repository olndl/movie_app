import 'dart:ui';

abstract class ColorsGuide {
  static Color titleColor = const Color(0xff000000);
  static Color bodyColor = const Color(0xff262626);
  static Color labelColor = const Color(0xff919090);
  static Color ligntColor = const Color(0xffd33d00);
  static Color appBarColor = const Color(0xFF90cea1);
  static Color primaryColor = const Color(0xFF0d253f);
  static Color lightBlue = const Color(0xFF01b4e4);
  static Color backgroundColor = const Color(0xFFF3F3F3);
  static Color cardColor = const Color(0xFFFFFFFF);
  static Color starColor = const Color(0xFFFFEA4A);
}
