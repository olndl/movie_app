import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movie_app/src/core/theme/colors_guide.dart';
import 'package:movie_app/providers/index_provider.dart';
import 'package:movie_app/src/core/navigation/model.dart';
import 'package:movie_app/src/core/navigation/provider.dart';

class CustomBottomNavBar extends ConsumerWidget {
  CustomBottomNavBar({Key? key}) : super(key: key);

  final List<BottomNavigationBarItem> allRoutes = [
    const BottomNavigationBarItem(
      icon: Icon(Icons.home),
      label: 'Home',
    ),
    const BottomNavigationBarItem(
      icon: Icon(Icons.bookmark),
      label: 'Favorite',
    ),
  ];

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final int menuIndex = ref.watch(indexProvider(0)).currentIndex;
    return Align(
      alignment: FractionalOffset.bottomCenter,
      child: ClipRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 10.0,
            sigmaY: 10.0,
          ),
          child: Opacity(
            opacity: 0.5,
            child: BottomNavigationBar(
              selectedItemColor: ColorsGuide.lightBlue,
              currentIndex: menuIndex,
              onTap: (int index) {
                ref.read(indexProvider(0)).indexSet(index);
                switch (index) {
                  case 0:
                    return ref.read(routerDelegateProvider).navigate([
                      ListMoviesSegment(),
                    ]);
                  case 1:
                    return ref.read(routerDelegateProvider).navigate([
                      ListFavoriteSegment(),
                    ]);
                }
              },
              type: BottomNavigationBarType.fixed,
              unselectedItemColor: ColorsGuide.titleColor,
              items: allRoutes,
            ),
          ),
        ),
      ),
    );
  }
}
