import 'package:flutter/material.dart';
import 'package:movie_app/src/core/extensions/extensions.dart';
import 'package:movie_app/src/core/theme/colors_guide.dart';

class CustomCard extends StatelessWidget {
  final double? width;
  final double? height;
  final Widget child;
  final bool isAdultContent;

  const CustomCard({
    Key? key,
    required this.child,
    this.width,
    this.height,
    this.isAdultContent = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: 1.percentOfHeight,
        horizontal: 5.percentOfWidth,
      ),
      width: width,
      height: height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: ColorsGuide.cardColor,
        boxShadow: [
          BoxShadow(
            color: ColorsGuide.labelColor.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: Stack(
        children: [
          child,
          if (isAdultContent)
            Positioned(
              child: Icon(
                Icons.eighteen_up_rating,
                color: ColorsGuide.ligntColor,
                size: 40,
              ),
            ),
        ],
      ),
    );
  }
}
