import 'package:intl/intl.dart';
import 'package:movie_app/src/features/domain/models/movies_model.dart';

class Utils {
  static String toFormatDateTime(DateTime date) {
    return DateFormat('dd MMM yyyy').format(date);
  }

  static DateTime? fromTimestampToDatetime(int? timestamp) => timestamp != null
      ? DateTime.fromMillisecondsSinceEpoch(timestamp * 1000)
      : null;

  static int? fromDatetimeToTimestamp(DateTime? date) =>
      date != null ? date.millisecondsSinceEpoch ~/ 100 : null;

  static List<MovieModel> updateMovie(
    MovieModel movie,
    List<MovieModel> list,
  ) {
    final listWithoutThisMovie =
        list.where((element) => element.id != movie.id).toList();
    final result = [...listWithoutThisMovie, movie];
    result.sort(
      (a, b) => a.title!.compareTo(b.title!),
    );
    return result;
  }
}
