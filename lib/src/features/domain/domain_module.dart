import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movie_app/src/features/data/data_module.dart';
import 'package:movie_app/src/features/domain/usecases/toggle_favorite_movie_usecase.dart';
import 'package:movie_app/src/features/domain/usecases/get_popular_movies_usecase.dart';

final getPopularMoviesUseCaseProvider = Provider<GetPopularMoviesUseCase>(
  (ref) => GetPopularMoviesUseCaseImpl(
    ref.watch(moviesRepositoryProvider),
  ),
);

final toggleFavoriteMoviesUseCaseProvider =
    Provider<ToggleFavoriteMoviesUseCase>(
  (ref) => ToggleFavoriteMoviesUseCaseImpl(
    ref.watch(moviesRepositoryProvider),
  ),
);
