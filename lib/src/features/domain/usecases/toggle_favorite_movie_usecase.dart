import 'package:movie_app/src/features/domain/models/movies_model.dart';
import 'package:movie_app/src/features/domain/repositories/movie_repository.dart';

abstract class ToggleFavoriteMoviesUseCase {
  Future<void> addFavoriteMovie(MovieModel favoriteMovie);
}

class ToggleFavoriteMoviesUseCaseImpl implements ToggleFavoriteMoviesUseCase {
  final MovieRepository _movieRepository;

  ToggleFavoriteMoviesUseCaseImpl(this._movieRepository);

  @override
  Future<void> addFavoriteMovie(MovieModel favoriteMovie) async {
    await _movieRepository.toggleMovie(favoriteMovie);
  }
}
