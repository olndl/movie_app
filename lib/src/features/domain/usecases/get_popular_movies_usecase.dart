import 'package:movie_app/src/features/domain/models/movies_model.dart';
import 'package:movie_app/src/features/domain/repositories/movie_repository.dart';

abstract class GetPopularMoviesUseCase {
  Future<List<MovieModel>> getPopularMovies();
}

class GetPopularMoviesUseCaseImpl implements GetPopularMoviesUseCase {
  final MovieRepository _movieRepository;

  GetPopularMoviesUseCaseImpl(this._movieRepository);

  @override
  Future<List<MovieModel>> getPopularMovies() {
    return _movieRepository.getPopularMovies();
  }
}
