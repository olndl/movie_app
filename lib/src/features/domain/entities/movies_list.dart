import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';

part 'movies_list.freezed.dart';

part 'movies_list.g.dart';

MoviesList moviesListFromJson(String str) =>
    MoviesList.fromJson(json.decode(str));

String moviesListToJson(MoviesList data) => json.encode(data.toJson());

@freezed
class MoviesList with _$MoviesList {
  const factory MoviesList({
    required int page,
    required List<Movie> results,
    @JsonKey(name: 'total_pages') required int? totalPages,
    @JsonKey(name: 'total_results') required int? totalResults,
  }) = _MoviesList;

  factory MoviesList.fromJson(Map<String, dynamic> json) =>
      _$MoviesListFromJson(json);
}

@freezed
class Movie with _$Movie {
  const factory Movie({
    required bool? adult,
    @JsonKey(name: 'backdrop_path') required String? backdropPath,
    required int id,
    @JsonKey(name: 'original_language') required String? originalLanguage,
    @JsonKey(name: 'original_title') required String? originalTitle,
    required String? overview,
    required double? popularity,
    @JsonKey(name: 'poster_path') required String? posterPath,
    @JsonKey(name: 'release_date') required DateTime? releaseDate,
    required String? title,
    required bool? video,
    @JsonKey(name: 'vote_average') required double? voteAverage,
    @JsonKey(name: 'vote_count') required int? voteCount,
  }) = _Movie;
  factory Movie.fromJson(Map<String, dynamic> json) => _$MovieFromJson(json);
}
