import 'package:freezed_annotation/freezed_annotation.dart';

part 'movies_model.freezed.dart';

part 'movies_model.g.dart';

@freezed
class MovieModel with _$MovieModel {
  const factory MovieModel({
    required bool? adult,
    @JsonKey(name: 'backdrop_path') required String? backdropPath,
    required int id,
    @JsonKey(name: 'original_language') required String? originalLanguage,
    @JsonKey(name: 'original_title') required String? originalTitle,
    required String? overview,
    required double? popularity,
    @JsonKey(name: 'poster_path') required String? posterPath,
    @JsonKey(name: 'release_date') required DateTime? releaseDate,
    required String? title,
    required bool? video,
    @JsonKey(name: 'vote_average') required double? voteAverage,
    @JsonKey(name: 'vote_count') required int? voteCount,
    required bool isFavorite,
  }) = _MovieModel;
  factory MovieModel.fromJson(Map<String, dynamic> json) =>
      _$MovieModelFromJson(json);
}
