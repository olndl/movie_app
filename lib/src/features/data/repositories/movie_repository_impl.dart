import 'package:movie_app/src/features/data/datasource/movie_datasource.dart';
import 'package:movie_app/src/features/domain/models/movies_model.dart';
import 'package:movie_app/src/features/domain/repositories/movie_repository.dart';

class MovieRepositoryImpl implements MovieRepository {
  final MovieDataSource movieDataSource;

  const MovieRepositoryImpl(this.movieDataSource);

  @override
  Future<List<MovieModel>> getPopularMovies() async {
    final List<MovieModel> sortedList;
    final moviesList = await movieDataSource.getPopularMovies();
    sortedList = moviesList.toList();
    sortedList.sort((a, b) => a.title!.compareTo(b.title!));
    return sortedList;
  }

  @override
  Future<void> toggleMovie(MovieModel movieModel) async {
    await movieDataSource.toggleMovie(movieModel);
  }
}
