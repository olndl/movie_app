import 'package:movie_app/src/core/api/dio_consumer.dart';
import 'package:movie_app/src/features/data/datasource/movie_datasource.dart';
import 'package:movie_app/src/features/domain/entities/movies_list.dart';
import 'package:movie_app/src/features/domain/models/movies_model.dart';

class RemoteMoviesDataSourceImpl implements MovieDataSource {
  DioConsumer dioConsumer;

  RemoteMoviesDataSourceImpl({
    required this.dioConsumer,
  });

  @override
  Future<List<MovieModel>> getPopularMovies() async {
    final moviesList = await dioConsumer.get('');
    final entitiesList = MoviesList.fromJson(moviesList).results;
    return entitiesList
        .map(
          (movie) => MovieModel(
            adult: movie.adult,
            backdropPath: movie.backdropPath,
            id: movie.id,
            originalLanguage: movie.originalLanguage,
            originalTitle: movie.originalTitle,
            overview: movie.overview,
            popularity: movie.popularity,
            posterPath: movie.posterPath,
            releaseDate: movie.releaseDate,
            title: movie.title,
            video: movie.video,
            voteAverage: movie.voteAverage,
            voteCount: movie.voteCount,
            isFavorite: false,
          ),
        )
        .toList();
  }

  @override
  Future<void> toggleMovie(MovieModel movieModel) {
    // TODO: implement toggleMovie
    throw UnimplementedError();
  }
}
