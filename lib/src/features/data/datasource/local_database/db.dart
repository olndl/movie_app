import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:movie_app/src/core/constants/database.dart';

class DB {
  static const databaseVersion = 4;
  static const columnAdult = 'adult';
  static const columnBackdropPath = 'backdrop_path';
  static const columnId = 'id';
  static const columnOriginalLanguage = 'original_language';
  static const columnOriginalTitle = 'original_title';
  static const columnOverview = 'overview';
  static const columnPopularity = 'popularity';
  static const columnPosterPath = 'poster_path';
  static const columnReleaseDate = 'release_date';
  static const columnTitle = 'title';
  static const columnVideo = 'video';
  static const columnVoteAverage = 'vote_average';
  static const columnVoteCount = 'vote_count';
  static const columnIsFavorite = 'is_favorite';

  DB._instance();

  static final DB db = DB._instance();
  late Database _database;

  Future<Database> get database async {
    _database = await _initDB();

    return _database;
  }

  Future<Database> _initDB() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String databasePath = directory.path + DataBase.databaseName;

    var db = await openDatabase(
      databasePath,
      version: databaseVersion,
      onCreate: _onCreate,
    );
    return db;
  }

  Future<void> _onCreate(Database db, int newVersion) async {
    await db.execute(
      '''
          CREATE TABLE ${DataBase.databaseName}(
            $columnAdult INTEGER NOT NULL,
            $columnBackdropPath TEXT,
            $columnId INTEGER NOT NULL,
            $columnOriginalLanguage TEXT,
            $columnOriginalTitle TEXT,
            $columnOverview TEXT,
            $columnPopularity REAL,
            $columnPosterPath TEXT,
            $columnReleaseDate INTEGER NOT NULL,
            $columnTitle TEXT,
            $columnVideo INTEGER NOT NULL,
            $columnVoteAverage REAL,
            $columnVoteCount INTEGER,
            $columnIsFavorite INTEGER NOT NULL
            )
        ''',
    );
  }

  Future close() async {
    var dbClient = await database;
    return dbClient.close();
  }
}
