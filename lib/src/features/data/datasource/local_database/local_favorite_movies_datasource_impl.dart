import 'package:movie_app/src/core/mappers/mapper.dart';
import 'package:movie_app/src/features/data/datasource/movie_datasource.dart';
import 'package:sqflite/sqflite.dart';
import 'package:movie_app/src/features/data/datasource/local_database/db.dart';
import 'package:movie_app/src/core/constants/database.dart';
import 'package:movie_app/src/features/domain/models/movies_model.dart';

class LocalMoviesDatasourceImpl implements MovieDataSource {
  var databaseFuture = DB.db.database;

  @override
  Future<List<MovieModel>> getPopularMovies() async {
    final Database database = await databaseFuture;
    final moviesMap = await database.query(DataBase.databaseName);
    final List<MovieModel> moviesList = List<MovieModel>.from(
      moviesMap.map(
        (favMovie) => Mapper().transformToMovieModel(favMovie),
      ),
    );
    return moviesList;
  }

  Future<void> saveMovie(MovieModel movie) async {
    final Database database = await databaseFuture;
    await database.transaction(
      (txn) async {
        final id = await txn.insert(
          DataBase.databaseName,
          Mapper().transformToMap(movie),
          conflictAlgorithm: ConflictAlgorithm.replace,
        );
        await txn.query(
          DataBase.databaseName,
          where: '${DB.columnId} = ?',
          whereArgs: [id],
        );
      },
    );
  }

  Future<void> updateMovie(MovieModel movie) async {
    final Database database = await databaseFuture;
    final int id = movie.id;
    await database.update(
      DataBase.databaseName,
      Mapper().transformToMap(movie),
      where: '${DB.columnId} = ?',
      whereArgs: [id],
    );
  }

  Future<void> deleteMovie(
    int id,
  ) async {
    final Database database = await databaseFuture;
    await database.delete(
      DataBase.databaseName,
      where: '${DB.columnId} = ?',
      whereArgs: [id],
    );
  }

  @override
  Future<void> toggleMovie(MovieModel movie) async {
    final newMovie = movie.copyWith(isFavorite: !movie.isFavorite);
    await updateMovie(newMovie);
  }
}
