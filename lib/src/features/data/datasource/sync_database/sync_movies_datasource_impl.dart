//SyncMoviesDatasourceImpl

import 'dart:io';

import 'package:movie_app/src/core/api/dio_consumer.dart';
import 'package:movie_app/src/features/data/datasource/local_database/local_favorite_movies_datasource_impl.dart';
import 'package:movie_app/src/features/data/datasource/movie_datasource.dart';
import 'package:movie_app/src/features/data/datasource/remote_database/remote_movies_datasource_impl.dart';

import 'package:movie_app/src/core/constants/endpoints.dart';
import 'package:movie_app/src/features/domain/models/movies_model.dart';

class SyncMoviesDatasourceImpl extends MovieDataSource {
  final LocalMoviesDatasourceImpl localMovies = LocalMoviesDatasourceImpl();
  final RemoteMoviesDataSourceImpl remoteMovies =
      RemoteMoviesDataSourceImpl(dioConsumer: DioConsumer());

  @override
  Future<List<MovieModel>> getPopularMovies() async {
    late List<MovieModel> currantLocalMovies;
    late List<MovieModel> currantRemoteMovies;
    currantLocalMovies = await localMovies.getPopularMovies();
    bool isOnline = await hasNetwork();
    if (isOnline && currantLocalMovies.isEmpty) {
      currantRemoteMovies = await remoteMovies.getPopularMovies();
      for (int i = 0; i < currantRemoteMovies.length; i++) {
        await localMovies.saveMovie(currantRemoteMovies[i]);
      }
      return currantRemoteMovies;
    } else if (isOnline) {
      currantRemoteMovies = await remoteMovies.getPopularMovies();
      //await mergeLists(currantLocalMovies, currantRemoteMovies);
      List<MovieModel> movies = await localMovies.getPopularMovies();
      return movies;
    } else {
      return currantLocalMovies;
    }
  }

  Future<bool> hasNetwork() async {
    try {
      final result = await InternetAddress.lookup(Endpoints.host);
      return result.isNotEmpty && result[0].rawAddress.isNotEmpty;
    } on SocketException catch (_) {
      return false;
    }
  }

  @override
  Future<void> toggleMovie(MovieModel movieModel) async {
    await localMovies.toggleMovie(movieModel);
  }
}
