import 'package:movie_app/src/features/domain/models/movies_model.dart';

abstract class MovieDataSource {
  Future<List<MovieModel>> getPopularMovies();
  Future<void> toggleMovie(MovieModel movieModel);
}
