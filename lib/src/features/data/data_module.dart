import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movie_app/src/features/data/datasource/movie_datasource.dart';
import 'package:movie_app/src/features/data/datasource/sync_database/sync_movies_datasource_impl.dart';
import 'package:movie_app/src/features/data/repositories/movie_repository_impl.dart';
import 'package:movie_app/src/features/domain/repositories/movie_repository.dart';

final moviesDatabaseProvider = Provider<MovieDataSource>(
  (_) => SyncMoviesDatasourceImpl(),
);
final moviesRepositoryProvider = Provider<MovieRepository>(
  (ref) => MovieRepositoryImpl(ref.watch(moviesDatabaseProvider)),
);
