import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movie_app/src/core/extensions/extensions.dart';
import 'package:movie_app/src/core/constants/interface.dart';
import 'package:movie_app/src/core/theme/colors_guide.dart';
import 'package:movie_app/src/features/presentation/homepage/presentation/components/custom_app_bar.dart';
import 'package:movie_app/src/features/presentation/homepage/presentation/components/navigation_panel.dart';
import 'package:movie_app/src/features/presentation/favorite/presentation/components/favorite_movies_list_widget.dart';
import 'package:movie_app/src/features/domain/models/movies_model.dart';
import 'package:movie_app/src/features/presentation/homepage/presentation/providers/movies_provider.dart';

class FavoritePageBody extends ConsumerWidget {
  final List<MovieModel> moviesList;

  const FavoritePageBody({Key? key, required this.moviesList})
      : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var moviesListState = ref.watch(moviesListStateNotifierProvider.notifier);
    return RefreshIndicator(
      strokeWidth: 2,
      color: ColorsGuide.appBarColor,
      onRefresh: () {
        return moviesListState.getPopularMoviesList();
      },
      child: CustomScrollView(
        physics: const BouncingScrollPhysics(),
        slivers: [
          const CustomAppBar(
            bar: NavigationPanel(
              Interface.favoriteTitle,
            ),
          ),
          SliverPadding(
            padding: EdgeInsets.only(top: 1.percentOfHeight),
          ),
          if (moviesList.isNotEmpty)
            FavoriteMoviesListWidget(
              favMoviesList: moviesList,
            )
        ],
      ),
    );
  }
}
