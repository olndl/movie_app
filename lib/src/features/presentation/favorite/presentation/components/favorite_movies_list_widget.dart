import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movie_app/src/core/widgets/click_style.dart';
import 'package:movie_app/src/core/navigation/model.dart';
import 'package:movie_app/src/core/navigation/provider.dart';
import 'package:movie_app/src/features/domain/models/movies_model.dart';
import 'package:movie_app/src/features/presentation/favorite/presentation/components/favorite_movie_card.dart';

class FavoriteMoviesListWidget extends ConsumerWidget {
  final List<MovieModel> favMoviesList;

  const FavoriteMoviesListWidget({
    required this.favMoviesList,
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return ClickStyle(
            onTap: () => {
              ref.read(routerDelegateProvider).navigate([
                ListMoviesSegment(),
                MovieDetailsSegment(
                  movie: favMoviesList[index],
                ),
              ]),
            },
            child: FavoriteMovieCard(
              favoriteMovie: favMoviesList[index],
            ),
          );
        },
        childCount: favMoviesList.length,
      ),
    );
  }
}
