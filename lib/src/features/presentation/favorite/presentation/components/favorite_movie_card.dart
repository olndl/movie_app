import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movie_app/src/core/theme/colors_guide.dart';
import 'package:movie_app/src/features/domain/models/movies_model.dart';
import 'package:movie_app/src/features/presentation/homepage/presentation/providers/movies_provider.dart';

class FavoriteMovieCard extends ConsumerWidget {
  final MovieModel favoriteMovie;

  const FavoriteMovieCard({
    super.key,
    required this.favoriteMovie,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ClipRRect(
      clipBehavior: Clip.hardEdge,
      child: Dismissible(
        key: ValueKey(
          favoriteMovie.id,
        ),
        background: Container(),
        secondaryBackground: Container(
          color: ColorsGuide.ligntColor,
          alignment: Alignment.lerp(
            Alignment.centerRight,
            Alignment.centerLeft,
            .05,
          ),
          child: Padding(
            padding: const EdgeInsets.all(15),
            child: Icon(
              Icons.delete_outline,
              color: ColorsGuide.cardColor,
            ),
          ),
        ),
        confirmDismiss: (direction) async {
          if (direction == DismissDirection.endToStart) {
            bool delete = true;
            final snackBarController =
                ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(
                  'Deleted ${favoriteMovie.title}',
                  maxLines: 1,
                ),
                action: SnackBarAction(
                  label: 'Undo',
                  onPressed: () => delete = false,
                ),
              ),
            );
            await snackBarController.closed;
            return delete;
          }
          return null;
        },
        onDismissed: (direction) {
          if (direction == DismissDirection.endToStart) {
            ref
                .read(moviesListStateNotifierProvider.notifier)
                .toggleFavoriteMovie(favoriteMovie);
          }
        },
        child: Card(
          child: ListTile(
            title: Text(favoriteMovie.title ?? 'unknown'),
          ),
        ),
      ),
    );
  }
}
