import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movie_app/src/core/screens/error_screen.dart';
import 'package:movie_app/src/core/screens/loading_screen.dart';
import 'package:movie_app/src/core/screens/unknown_screen.dart';
import 'package:movie_app/src/core/theme/colors_guide.dart';
import 'package:movie_app/src/core/widgets/custom_bottom_navigation_bar.dart';
import 'package:movie_app/src/features/presentation/favorite/presentation/components/favorite_page_body.dart';
import 'package:movie_app/src/features/presentation/homepage/presentation/providers/movies_provider.dart';

class FavoritePage extends ConsumerWidget {
  FavoritePage({Key? key}) : super(key: key);

  final _moviesListProvider = moviesListStateNotifierProvider;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      backgroundColor: ColorsGuide.backgroundColor,
      body: ref.watch(_moviesListProvider).maybeWhen(
            success: (content) {
              return Stack(
                children: [
                  FavoritePageBody(
                    moviesList: content.isNotEmpty
                        ? content
                            .where((element) => element.isFavorite == true)
                            .toList()
                        : [],
                  ),
                  CustomBottomNavBar(),
                ],
              );
            },
            error: (e) => ErrorScreen(
              message: '$e',
            ),
            loading: () => const LoadingScreen(),
            orElse: () => const UnknownScreen(),
          ),
    );
  }
}
