import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movie_app/src/core/extensions/extensions.dart';
import 'package:movie_app/src/core/theme/colors_guide.dart';
import 'package:movie_app/src/core/widgets/click_style.dart';
import 'package:movie_app/src/features/domain/models/movies_model.dart';
import 'package:movie_app/src/features/presentation/homepage/presentation/providers/movies_provider.dart';

class FavoritesButton extends ConsumerWidget {
  final MovieModel movie;

  const FavoritesButton(
    this.movie, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isFavorite = ref
        .watch(moviesListStateNotifierProvider.notifier)
        .isFavoriteValue(movie.id);
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(60.percentOfHeight),
      ),
      elevation: 4,
      child: ClickStyle(
        onTap: () {
          ref
              .read(moviesListStateNotifierProvider.notifier)
              .toggleFavoriteMovie(movie);
        },
        indent: 10,
        child: isFavorite
            ? Icon(
                Icons.bookmark,
                color: ColorsGuide.ligntColor,
              )
            : Icon(
                Icons.bookmark_border,
                color: ColorsGuide.primaryColor,
              ),
      ),
    );
  }
}
