import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movie_app/src/core/extensions/extensions.dart';
import 'package:movie_app/src/core/widgets/gap_w.dart';
import 'package:movie_app/src/core/constants/endpoints.dart';
import 'package:movie_app/src/core/theme/colors_guide.dart';
import 'package:movie_app/src/core/theme/typography.dart';
import 'package:movie_app/src/core/widgets/custom_card.dart';
import 'package:movie_app/src/core/utils/utils.dart';
import 'package:movie_app/src/core/widgets/nothing.dart';
import 'package:movie_app/src/features/domain/models/movies_model.dart';

class MovieTitleCard extends ConsumerWidget {
  final MovieModel movie;

  const MovieTitleCard({Key? key, required this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SliverToBoxAdapter(
      child: Padding(
        padding: EdgeInsets.only(top: 7.percentOfHeight),
        child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            CustomCard(
              width: 90.percentOfWidth,
              height: 25.percentOfHeight,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 3.percentOfWidth,
                  vertical: 2.percentOfHeight,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      movie.title ?? 'unknown',
                      style: TextStyles.body
                          .copyWith(fontSize: 18, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                      maxLines: 2,
                    ),
                    Text(
                      movie.originalTitle ?? 'unknown',
                      style: TextStyles.body.copyWith(
                        fontSize: 14,
                      ),
                      textAlign: TextAlign.center,
                      maxLines: 2,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.star_rounded,
                              size: 40,
                              color: ColorsGuide.starColor,
                            ),
                            Text(
                              '${movie.voteAverage} /10',
                              style: TextStyles.body.copyWith(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                              maxLines: 2,
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            movie.originalLanguage != null
                                ? Icon(
                                    Icons.language,
                                    color: ColorsGuide.primaryColor,
                                  )
                                : const Nothing(),
                            const GapW(
                              param: .3,
                            ),
                            movie.originalLanguage != null
                                ? Text(
                                    movie.originalLanguage!.toUpperCase(),
                                    style: TextStyles.body,
                                    maxLines: 1,
                                  )
                                : const Nothing(),
                          ],
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.date_range,
                              color: ColorsGuide.primaryColor,
                            ),
                            const GapW(
                              param: 1,
                            ),
                            Text(
                              movie.releaseDate != null
                                  ? Utils.toFormatDateTime(movie.releaseDate!)
                                  : 'none',
                              style: TextStyles.smallBody
                                  .copyWith(color: ColorsGuide.titleColor),
                            ),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 1.percentOfHeight,
              child: FractionalTranslation(
                translation: const Offset(0.0, -0.4),
                child: Align(
                  alignment: const FractionalOffset(0.5, 0.0),
                  child: Center(
                    child: CircleAvatar(
                      radius: 20.percentOfWidth,
                      backgroundColor: ColorsGuide.appBarColor,
                      child: CachedNetworkImage(
                        imageUrl: '${Endpoints.imageUrl}${movie.posterPath}',
                        imageBuilder: (context, imageProvider) => Container(
                          width: 39.percentOfWidth,
                          height: 39.percentOfWidth,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        placeholder: (context, url) =>
                            const CircularProgressIndicator(),
                        errorWidget: (context, url, error) =>
                            const Icon(Icons.error),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
