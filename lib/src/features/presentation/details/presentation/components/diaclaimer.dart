import 'package:flutter/material.dart';
import 'package:movie_app/src/core/extensions/extensions.dart';
import 'package:movie_app/src/core/theme/colors_guide.dart';
import 'package:movie_app/src/core/theme/typography.dart';
import 'package:movie_app/src/core/widgets/custom_card.dart';

class Disclaimer extends StatelessWidget {
  const Disclaimer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: CustomCard(
        width: 90.percentOfWidth,
        height: 15.percentOfHeight,
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 5.percentOfWidth,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Icon(
                Icons.error,
                color: ColorsGuide.ligntColor,
              ),
              Text(
                'This movie contains explicit material that is intended for adults some items within the movie may contain or depict acts of extreme violence',
                style: TextStyles.body.copyWith(
                  color: ColorsGuide.ligntColor,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
