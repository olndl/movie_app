import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movie_app/src/core/extensions/extensions.dart';
import 'package:movie_app/src/core/navigation/model.dart';
import 'package:movie_app/src/core/theme/colors_guide.dart';
import 'package:movie_app/src/core/constants/interface.dart';
import 'package:movie_app/src/core/navigation/provider.dart';
import 'package:movie_app/src/core/theme/typography.dart';

class DetailsPanel extends ConsumerWidget {
  const DetailsPanel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Container(
      padding: EdgeInsets.only(right: 3.percentOfWidth),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              IconButton(
                onPressed: () {
                  ref
                      .read(routerDelegateProvider)
                      .navigate([ListMoviesSegment()]);
                },
                icon: const Icon(
                  Icons.arrow_back_ios_new_outlined,
                  size: 15,
                ),
              ),
            ],
          ),
          Text(
            Interface.detailsTitle,
            style: TextStyles.title.copyWith(color: ColorsGuide.primaryColor),
          ),
        ],
      ),
    );
  }
}
