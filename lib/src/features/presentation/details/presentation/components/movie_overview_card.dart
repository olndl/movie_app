import 'package:flutter/cupertino.dart';
import 'package:movie_app/src/core/extensions/extensions.dart';
import 'package:movie_app/src/core/theme/typography.dart';
import 'package:movie_app/src/core/widgets/custom_card.dart';

class MovieOverviewCard extends StatelessWidget {
  final String? overview;

  const MovieOverviewCard({Key? key, this.overview}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: CustomCard(
        width: 90.percentOfWidth,
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 3.percentOfWidth,
            vertical: 2.percentOfHeight,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Overview',
                style: TextStyles.title,
              ),
              Text(
                overview ?? 'none',
                style: TextStyles.body,
              )
            ],
          ),
        ),
      ),
    );
  }
}
