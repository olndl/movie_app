import 'package:flutter/material.dart';
import 'package:movie_app/src/core/widgets/custom_bottom_navigation_bar.dart';
import 'package:movie_app/src/core/theme/colors_guide.dart';
import 'package:movie_app/src/features/domain/models/movies_model.dart';
import 'package:movie_app/src/features/presentation/details/presentation/components/diaclaimer.dart';
import 'package:movie_app/src/features/presentation/homepage/presentation/components/custom_app_bar.dart';
import 'package:movie_app/src/features/presentation/details/presentation/components/details_panel.dart';
import 'package:movie_app/src/features/presentation/details/presentation/components/movie_title_card.dart';
import 'package:movie_app/src/features/presentation/details/presentation/components/movie_overview_card.dart';

class DetailsPage extends StatelessWidget {
  final MovieModel movie;

  const DetailsPage({Key? key, required this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorsGuide.backgroundColor,
      body: Stack(
        children: [
          RefreshIndicator(
            child: CustomScrollView(
              slivers: [
                const CustomAppBar(
                  bar: DetailsPanel(),
                ),
                MovieTitleCard(
                  movie: movie,
                ),
                if (movie.adult ?? false) const Disclaimer(),
                MovieOverviewCard(
                  overview: movie.overview,
                ),
              ],
            ),
            onRefresh: () async => 0,
          ),
          CustomBottomNavBar()
        ],
      ),
    );
  }
}
