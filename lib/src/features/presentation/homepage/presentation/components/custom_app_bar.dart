import 'package:flutter/material.dart';
import 'package:movie_app/src/core/theme/colors_guide.dart';
import 'package:movie_app/src/core/widgets/nothing.dart';
import 'package:movie_app/src/gen/assets.gen.dart';

class CustomAppBar extends StatelessWidget {
  final Widget bar;

  const CustomAppBar({Key? key, required this.bar}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      stretch: true,
      elevation: 2,
      onStretchTrigger: () {
        return Future<void>.value();
      },
      leading: const Nothing(),
      expandedHeight: 150.0,
      backgroundColor: ColorsGuide.backgroundColor,
      flexibleSpace: FlexibleSpaceBar(
        stretchModes: const <StretchMode>[
          StretchMode.zoomBackground,
          StretchMode.blurBackground,
          StretchMode.fadeTitle,
        ],
        centerTitle: true,
        title: bar,
        background: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: DecoratedBox(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(50.0),
                  ),
                  //color: ColorsGuide.appBarColor,
                ),
                child: Assets.lib.src.assets.svg.blueLong.svg(
                  colorFilter: ColorFilter.mode(
                    ColorsGuide.appBarColor,
                    BlendMode.srcATop,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
