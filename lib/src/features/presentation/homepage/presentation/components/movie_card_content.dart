import 'package:flutter/cupertino.dart';
import 'package:movie_app/src/core/extensions/extensions.dart';

class MovieCardContent extends StatelessWidget {
  const MovieCardContent({
    Key? key,
    this.contentPadding,
    this.title,
  }) : super(key: key);

  final EdgeInsetsGeometry? contentPadding;

  final Widget? title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: contentPadding ??
          EdgeInsets.only(
            top: 1.percentOfHeight,
            bottom: 1.5.percentOfHeight,
            left: 4.percentOfWidth,
            right: 4.percentOfWidth,
          ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          if (title != null)
            Padding(
              padding: EdgeInsets.only(top: 1.percentOfHeight),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (title != null) title!,
                ],
              ),
            ),
        ],
      ),
    );
  }
}
