import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:movie_app/src/core/widgets/custom_card.dart';
import 'package:movie_app/src/core/widgets/nothing.dart';
import 'package:movie_app/src/features/presentation/homepage/presentation/components/movie_card_content.dart';

class MovieCard extends StatelessWidget {
  const MovieCard({
    Key? key,
    this.width,
    this.height,
    this.heightImage,
    this.borderRadius = 10,
    this.contentPadding,
    this.poster,
    this.title,
    this.color = Colors.white,
    this.isAdult = false,
  }) : super(key: key);

  final double? width;
  final double? height;
  final double? heightImage;
  final double borderRadius;
  final EdgeInsetsGeometry? contentPadding;
  final String? poster;
  final Color color;
  final Widget? title;
  final bool isAdult;

  @override
  Widget build(BuildContext context) {
    return CustomCard(
      isAdultContent: isAdult,
      width: width,
      height: height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          poster != null
              ? ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(borderRadius),
                    topRight: Radius.circular(borderRadius),
                  ),
                  child: CachedNetworkImage(
                    imageUrl: poster!,
                    fit: BoxFit.fill,
                    width: width,
                    height: heightImage,
                    progressIndicatorBuilder:
                        (context, url, downloadProgress) =>
                            CircularProgressIndicator(
                      value: downloadProgress.progress,
                    ),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  ),
                )
              : const Nothing(),
          MovieCardContent(
            contentPadding: contentPadding,
            title: title,
          ),
        ],
      ),
    );
  }
}
