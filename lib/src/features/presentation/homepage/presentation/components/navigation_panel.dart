import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movie_app/src/core/theme/typography.dart';
import 'package:movie_app/src/core/theme/colors_guide.dart';

class NavigationPanel extends ConsumerWidget {
  final String text;
  const NavigationPanel(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Text(
      text,
      style: TextStyles.title.copyWith(
        color: ColorsGuide.primaryColor,
        fontSize: 25,
      ),
    );
  }
}
