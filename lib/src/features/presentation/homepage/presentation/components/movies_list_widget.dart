import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movie_app/src/core/constants/endpoints.dart';
import 'package:movie_app/src/core/extensions/extensions.dart';
import 'package:movie_app/src/core/navigation/model.dart';
import 'package:movie_app/src/core/theme/colors_guide.dart';
import 'package:movie_app/src/core/widgets/click_style.dart';
import 'package:movie_app/src/features/domain/models/movies_model.dart';
import 'package:movie_app/src/features/presentation/details/presentation/components/favorites_button.dart';
import 'package:movie_app/src/features/presentation/homepage/presentation/components/movie_card.dart';
import 'package:movie_app/src/core/navigation/provider.dart';
import 'package:movie_app/src/core/theme/typography.dart';

class MoviesListWidget extends ConsumerWidget {
  final List<MovieModel> moviesList;

  const MoviesListWidget({
    required this.moviesList,
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          final imageContentPath = moviesList[index].posterPath;
          final titleContent = moviesList[index].title;
          final year = moviesList[index].releaseDate?.year;
          return ClickStyle(
            onTap: () => {
              ref.read(routerDelegateProvider).navigate([
                ListMoviesSegment(),
                MovieDetailsSegment(
                  movie: moviesList[index],
                ),
              ]),
            },
            child: Stack(
              children: [
                MovieCard(
                  isAdult: moviesList[index].adult ?? false,
                  width: 90.percentOfWidth,
                  heightImage: 60.percentOfHeight,
                  poster: '${Endpoints.imageUrl}$imageContentPath',
                  title: Text(
                    '$titleContent ($year)',
                    style: TextStyles.title.copyWith(
                      color: ColorsGuide.primaryColor,
                    ),
                    maxLines: 2,
                  ),
                ),
                Positioned(
                  top: 2.percentOfHeight,
                  right: 8.percentOfWidth,
                  child: FavoritesButton(
                    moviesList[index],
                  ),
                )
              ],
            ),
          );
        },
        childCount: moviesList.length,
      ),
    );
  }
}
