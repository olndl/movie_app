import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movie_app/src/core/state/state.dart';
import 'package:movie_app/src/features/domain/models/movies_model.dart';
import 'package:movie_app/src/features/domain/usecases/get_popular_movies_usecase.dart';
import 'package:movie_app/src/features/domain/domain_module.dart';
import 'package:movie_app/src/features/domain/usecases/toggle_favorite_movie_usecase.dart';

final moviesListStateNotifierProvider =
    StateNotifierProvider.autoDispose<MovieListModel, State<List<MovieModel>>>(
        (ref) {
  return MovieListModel(
    ref.watch(getPopularMoviesUseCaseProvider),
    ref.watch(toggleFavoriteMoviesUseCaseProvider),
  );
});

class MovieListModel extends StateNotifier<State<List<MovieModel>>> {
  final GetPopularMoviesUseCase _getPopularMoviesUseCase;
  final ToggleFavoriteMoviesUseCase _toggleFavoriteMoviesUseCase;

  MovieListModel(
    this._getPopularMoviesUseCase,
    this._toggleFavoriteMoviesUseCase,
  ) : super(const State.init()) {
    getPopularMoviesList();
  }

  isFavoriteValue(int id) =>
      state.data!.firstWhere((element) => element.id == id).isFavorite;

  Future<void> getPopularMoviesList() async {
    try {
      state = const State.loading();
      final moviesList = await _getPopularMoviesUseCase.getPopularMovies();
      state = State.success(
        moviesList,
      );
    } on Exception catch (e) {
      state = State.error(e);
    }
  }

  Future<void> toggleFavoriteMovie(MovieModel movieModel) async {
    await _toggleFavoriteMoviesUseCase.addFavoriteMovie(movieModel);
    await getPopularMoviesList();
  }
}
