import 'package:movie_app/src/app/movie_app.dart';
import 'package:movie_app/src/core/errors/logger.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() {
  runApp(const ProviderScope(child: MovieApp()));
  initLogger();
  logger.info('start main...');
}
